import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by philip on 11/29/16.
 */
public class TestModelTestStep {
    public List<String> params;
    public String action;
    public String description;
    public int index;

    private Map<String, String> properties;
    static final Pattern pattern = Pattern.compile("^\\{([^\\}]+)\\}$");

    TestModelTestStep(int index, String action, String params, String description, Map<String, String> properties) {
        this.index = index;
        this.action = action;
        this.params = new ArrayList<>(Arrays.asList(params.split(",")));
        this.description = description;
        this.properties = properties;
    }

    public void validate(Class c) throws Exception {
        Method method = null;

        Method methods[] = c.getDeclaredMethods();
        for (Method m : methods) {
            if (m.getName().equalsIgnoreCase(this.action)) {
                method = m;
                break;
            }
        }

        if (method == null) {
            throw new Exception("方法 " + this.action + " 不存在");
        }

        int methodParamCount = method.getParameterCount();
        if (params.size() != methodParamCount) {
            throw new Exception("方法 " + this.action + " 参数不正确，应该有" + methodParamCount + "个，案例传入" + params.size() + "个");
        }

        for (int i = 0; i < params.size(); i ++) {
            String param = params.get(i);

            Matcher matcher = pattern.matcher(param);

            if (matcher.find()) {
                String foundMatchGroupKey = matcher.group(1);
                if (!this.properties.containsKey(foundMatchGroupKey)) {
                    throw new Exception("替换参数 " + foundMatchGroupKey + " 的值不存在");
                }

                String value = this.properties.get(foundMatchGroupKey);
                if (value.isEmpty()) {
                    throw new Exception("替换参数 " + foundMatchGroupKey + "的值不能为空");
                }

                params.remove(i);
                params.add(i, value);
            }
        }
    }
}
