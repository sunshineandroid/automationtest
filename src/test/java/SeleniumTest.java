import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by philip on 11/29/16.
 */
public class SeleniumTest extends BaseTest {
    private static WebDriver webdriver;

    SeleniumTest(String testId, String testName) {
        super(testId, testName);
    }

    public void open(String driverName) {
        if (driverName.equalsIgnoreCase("firefox")) {
            webdriver = new FirefoxDriver();
        } else {
            assertTrue(false, "不支持此驱动: " + driverName);
        }
    }

    public void load(String url) {
        log("加载网页 [" + url +"]");
        webdriver.get(url);
    }

    public void click_by_linktext(String name) {
        log ("点击链接文字 [" + name + "]");
        WebElement e = (WebElement)createWaitObject().until(ExpectedConditions.elementToBeClickable(By.linkText(name)));
        e.click();
    }

    public void click_by_xpath(String xpath) {
        log ("点击xpath链接 [" + xpath + "]");
        WebElement e = (WebElement)createWaitObject().until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        e.click();
    }

    public void enter_text_by_id(String id, String text) {
        log("用id查找 [" + id + "] 然后输入 [" + text + "]");
        WebElement e = (WebElement)createWaitObject().until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
        e.sendKeys(text);
    }

    public void select_dropdown_by_xpath(String xpath, String option) {
        log("用xpath查找 [" + xpath + "] 然后选择选项 [" + option + "]");
        WebElement e = (WebElement)createWaitObject().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        Select select = new Select(e);
        select.selectByVisibleText(option);
    }

    public void verify_title(String title) {
        log("确认题目为 [" + title + "]");
        createWaitObject().until(ExpectedConditions.titleContains(title));
    }

    public void verify_text_by_xpath(String xpath, String text) {
        log("用xpath查找 [" + xpath + "] 然后确认文本 [" + text + "]");
        createWaitObject().until(ExpectedConditions.textToBe(By.xpath(xpath), text));
    }

    public void verify_partial_text_by_xpath(String xpath, String text) {
        log("用xpath查找 [" + xpath + "] 然后确认部分文本 [" + text + "]");
        createWaitObject().until(ExpectedConditions.textMatches(By.xpath(xpath), Pattern.compile(".*" + text + ".*")));
    }

    public void verify_attribute_by_xpath(String xpath, String attribute, String value) {
        log("用xpath查找 [" + xpath + "] 然后确认元素属性 [" + attribute + "] 的内容 [" + value + "]");
        createWaitObject().until(ExpectedConditions.attributeToBe(By.xpath(xpath), attribute, value));
    }

    public void hard_wait(String waitTime) throws Exception {
        log("强行等待 [" + waitTime + "] 豪秒");
        Thread.sleep(new Long(waitTime));
    }

    private Wait createWaitObject() {
        return new FluentWait(webdriver).withTimeout(10, TimeUnit.SECONDS).pollingEvery(250, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
    }

    @Override
    protected void tearDown() {
        super.tearDown();

        webdriver.close();
        webdriver.quit();
    }
}
