import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.*;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by philip on 11/23/16.
 */
public class MasterTestCase {
    private final String propertiesFileName = "params.properties";
    private final String masterTestFileName = "MasterBoot.xlsx";
    private final int TestStepNumberOfColumns = 3;
    private Map<String, String> properties = new HashMap<>();
    private String testPath = null;

    MasterTestCase() throws Exception {
        testPath = System.getProperty("testPath");

        if (testPath.isEmpty()) {
            throw new Exception("参数 testPath 找不到，用 -DtestPath=xxx 传入");
        }

        String propertyFilePath = testPath + "/" + propertiesFileName;

        try {
            Properties prop = new Properties();
            prop.load(new InputStreamReader(new FileInputStream(propertyFilePath), "UTF-8"));
            Enumeration<?> e = prop.keys();
            while(e.hasMoreElements()) {
                String key = (String)e.nextElement();
                String value = prop.getProperty(key, "");

                properties.put(key, value);
            }
        } catch (Exception e) {
            System.err.println("无法读取参数配置文件: " + propertyFilePath);
        }
    }

    @TestFactory
    Collection<DynamicTest> boot() {
        Collection<DynamicTest> tests = new ArrayList<>();

        Collection<TestModelModule> moduleList = getModuleList();
        System.out.println("Processed " + moduleList.size() + " modules");

        moduleList.forEach((v) -> {
            Collection<TestModelTestCase> testCastList = getTestCaseList(v);
            testCastList.forEach((c) -> {
                tests.add(getDynamicTestFromModuleAndTestCase(v, c));
            });
        });

        return tests;
    }

    private String getTestNameFromModuleAndTestCase(TestModelModule module, TestModelTestCase testCase) {
        return module.moduleName + "_" + testCase.functionName + "_" + testCase.caseName;
    }

    private String getTestIDFromModuleAndTestCase(TestModelModule module, TestModelTestCase testCase) {
        return module.moduleId + "_" + testCase.caseId;
    }

    private DynamicTest getDynamicTestFromModuleAndTestCase(TestModelModule module, TestModelTestCase testCase) {
        String filePath = testPath + "/modules/cases/" + module.moduleId + "_" + testCase.caseId + ".xlsx";
        String testName = getTestNameFromModuleAndTestCase(module, testCase);
        String testId = getTestIDFromModuleAndTestCase(module, testCase);

        try {
            SeleniumTest test = new SeleniumTest(testId, testName);
            Workbook stepWorkbook = getWorkbookByFilename(filePath);
            Sheet sheet = stepWorkbook.getSheetAt(0);
            Iterator<Row> iter = sheet.rowIterator();
            iter.next(); // Skip header

            int index = 1;
            while(iter.hasNext()) {
                Row row = iter.next();

                if (row.getPhysicalNumberOfCells() != TestStepNumberOfColumns) {
                    throw new Exception("测试用例格式不对，应该为" + TestStepNumberOfColumns +"列");
                }

                String stepAction = row.getCell(0, Row.RETURN_NULL_AND_BLANK).getStringCellValue();
                String stepParams = row.getCell(1, Row.RETURN_NULL_AND_BLANK).getStringCellValue();
                String stepDescription = row.getCell(2, Row.RETURN_NULL_AND_BLANK).getStringCellValue();

                test.addStep(index++, stepAction, stepParams, stepDescription, this.properties);

                try {
                    test.validate();
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception("测试用例处理失败: " + stepAction + " " + stepParams + ": " + e.getLocalizedMessage());
                }
            }

            return DynamicTest.dynamicTest(testName, ()->{
                test.run();
                assertTrue(true);
            });
        } catch (Exception e) {
            return DynamicTest.dynamicTest(testName, ()->{
               assertTrue(false,  testId + " (" + testName + ") 运行失败: " + e.getLocalizedMessage());
            });
        }
    }


    private Collection<TestModelTestCase> getTestCaseList(TestModelModule module) {
        Collection<TestModelTestCase> testCaseList = new ArrayList<>();
        String moduleName = module.moduleName;
        String moduleId = module.moduleId;
        String filePath = testPath + "/modules/" + moduleId + ".xlsx";

        try {
            Workbook masterWorkbook = getWorkbookByFilename(filePath);
            int sheetCount = masterWorkbook.getNumberOfSheets();

            for (int i = 0; i < sheetCount; i++) {
                Sheet sheet = masterWorkbook.getSheetAt(i);
                String functionName = sheet.getSheetName();

                Iterator<Row> iter = sheet.rowIterator();
                iter.next(); // Skip header

                while(iter.hasNext()) {
                    Row row = iter.next();

                    String testId = row.getCell(0).getStringCellValue();
                    String testEnabled = row.getCell(1).getStringCellValue();
                    String testName = row.getCell(2).getStringCellValue();

                    if (testEnabled.equalsIgnoreCase("Y")) {
                        testCaseList.add(new TestModelTestCase(testId, testEnabled, testName, functionName));
                    }
                }
            }

            return testCaseList;
        } catch (Exception e) {
            System.err.println("Failed with " + moduleId + "(" + moduleName + "): " + e.getLocalizedMessage());
        }

        return testCaseList;
    }

    private Collection<TestModelModule> getModuleList() {
        Collection<TestModelModule> moduleList = new ArrayList<>();

        String masterListFileName = testPath + "/" + masterTestFileName;

        try {
            Workbook masterWorkbook = getWorkbookByFilename(masterListFileName);
            Sheet firstSheet = masterWorkbook.getSheetAt(0);
            Iterator<Row> iter = firstSheet.rowIterator();
            iter.next(); // Skip header

            while (iter.hasNext()) {
                Row row = iter.next();

                String moduleId = row.getCell(0).getStringCellValue();
                String moduleEnabled = row.getCell(1).getStringCellValue();
                String moduleName = row.getCell(2).getStringCellValue();

                if (moduleEnabled.equalsIgnoreCase("Y")) {
                    moduleList.add(new TestModelModule(moduleId, moduleEnabled, moduleName));
                }
            }
        } catch (Exception e) {
            System.err.println("Unable to get module list: " + e.getLocalizedMessage());
        }

        return moduleList;
    }

    private Workbook getWorkbookByFilename(String filePath) throws Exception {
        try {
            return new XSSFWorkbook(new FileInputStream(filePath));
        } catch (Exception e) {
            throw new Exception("文件不存在 " + filePath);
        }
    }
}
