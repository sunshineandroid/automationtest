import org.opentest4j.AssertionFailedError;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by philip on 11/23/16.
 */
public class BaseTest {
    private String testId;
    private String testName;
    private List<TestModelTestStep> steps = new ArrayList<>();

    public BaseTest(String testId, String testName) {
        this.testId = testId;
        this.testName = testName;
    }

    public void addStep(int index, String action, String params, String description, Map<String, String> properties) {
        if (!action.isEmpty()) {
            steps.add(new TestModelTestStep(index, action, params, description, properties));
        }
    }

    public void validate() throws Exception {
        for (TestModelTestStep step : steps) {
            step.validate(this.getClass());
        }
    }

    public void run() throws Throwable {
        log("运行 [" + testName + "] - [" + testId + "]");

        for (TestModelTestStep step : steps) {
            runStep(step);
        }

        tearDown();
    }

    public void runStep(TestModelTestStep step) throws Throwable {
        Method method = null;

        Method methods[] = this.getClass().getDeclaredMethods();
        for (Method m : methods) {
            if (m.getName().equalsIgnoreCase(step.action)) {
                method = m;
                break;
            }
        }

        try {
            method.invoke(this, step.params.toArray());
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            tearDown();

            if (e.getCause().getClass().getSimpleName().equalsIgnoreCase(AssertionFailedError.class.getSimpleName())) {
                throw e.getCause();
            } else {
                throw new Exception("执行 " + step.action + " (第" + step.index + "行方法) 错误: " + e.getCause().getLocalizedMessage());
            }
        }
    }

    protected void tearDown() {}

    protected void log(String message) {
        System.out.println(" +++ " + message);
    }
}