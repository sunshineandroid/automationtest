/**
 * Created by philip on 11/23/16.
 */
public class TestModelTestCase {
    public String caseId;
    public String caseEnabled;
    public String caseName;
    public String functionName;

    TestModelTestCase(String caseId, String caseEnabled, String caseName, String functionName) {
        this.caseEnabled = caseEnabled;
        this.caseId = caseId;
        this.caseName = caseName;
        this.functionName = functionName;
    }
}
