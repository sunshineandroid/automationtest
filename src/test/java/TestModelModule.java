/**
 * Created by philip on 11/23/16.
 */
public class TestModelModule {
    public String moduleId;
    public String moduleEnabled;
    public String moduleName;

    TestModelModule(String moduleId, String moduleEnabled, String moduleName) {
        this.moduleEnabled = moduleEnabled;
        this.moduleId = moduleId;
        this.moduleName = moduleName;
    }
}
